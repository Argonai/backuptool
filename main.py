from shutil import copytree, make_archive, copy, ignore_patterns, move, rmtree, unpack_archive
import PySimpleGUI as sg
import os
import json
path_list = []
extensions_list = []
sg.theme('DefaultNoMoreNagging')
export_column = [ #Everything in the same brackets will appear one one line, a new bracket means the item will appear blow the first one
    [
        sg.Text("Folders to export"),
        ],
    [
        sg.Input(key="-INPUT-"), sg.FolderBrowse() #Opens explorer for selecting folder
        ],
    [
        sg.Button("Add Folder"), sg.Cancel() #event for adding folder to list
        ],
    [
        sg.Text('Add filetypes to ignore (.png, .jpg, .pdf,...)')
        ],
    [
        sg.InputText(key="-EXTENSIONS INPUT-"), sg.Button("Add Extension")
        ],
    [
        sg.Listbox(values=[], enable_events=True, size=(40,20), key="-FILE LIST-"), sg.Listbox(values=[], enable_events=True, size=(40,20), key="-EXTENSIONS LIST-") #list of items
        ],#2 boxes, one for added folders and one for extensions to ignore
    [
        sg.Button("Export")
        ]
    ]
import_column = [
    [sg.Text("Restore from backup")],
    [sg.Input(key="-BACKUP LOCATION-"), sg.FileBrowse()], #opens explorer for selecting files
    [sg.Button("Restore")], #button to start restoring 
    [sg.Output(size=(60,20))]] #output where the process will be printed


layout = [ #combines both layouts with a separator between columns

    [
        sg.Column(export_column),
        sg.VSeperator(),
        sg.Column(import_column),
    ]

]
window = sg.Window("Backuptool", layout) #makes window object
def export(p_list, e_list):
    data = {}
       
    #accepting_input = 1
    #while(accepting_input == 1):
    #    path = ""
    #    path = input()
    #    if(path == "exit"):
    #        print("exit read")
    #        accepting_input = 0
    #        break
    #    else:
    #        path_list.append(path)
    #print("test")
    
    #paths_file = open("paths.txt", "r")
    #p_list = paths_file.read().splitlines()
    #paths_file.close()
    
    #extensions_file = open("extensions_to_ignore.txt", "r")
    #extensions_list = extensions_file.read().splitlines()
    #extensions_file.close()
    
    home = os.path.expanduser('~') #maps home directory to variable
    os.chdir(home + '\\Documents') #go to documents folder
    print("Creating backup folder")

    try: #tries to create mackup folder, if one already exists it will be overridden
        os.mkdir('backup')
    except:
        print("Backup folder already exists, overriding")
        #print("A backup folder already exists, override the directory?(Y/N)")
        #answer = input().lower()
        #if(answer == "y"):
        #    rmtree('backup')
        #    os.mkdir('backup')
        #elif(answer =="n"):
        #    print('exiting program')
        #    exit(1)
        rmtree(home + '\\Documents\\backup') #removes existing folder
        os.mkdir('backup') #makes new folder
    print("Backup folder created")

    for paths in p_list: #goes over every path and copies it to the backup folder
        
        name = os.path.basename(paths) 
        data[name] = paths
        dst_dir = home + '\\Documents\\backup\\' + name
        if os.path.isdir(paths):
            print(f"added folder {name}")
            copytree(paths, dst_dir, ignore = ignore_patterns(*e_list))
        else:
            print("added file") #old code, still keeping it, might be usefull
            copy(paths, dst_dir)

    os.chdir(f'{home}\\Documents')
    print(data)
    print("Creating json file")
    with open("destination.json", "w") as file: #creates json file with where the files where stored for restoring
        json.dump(data, file, indent=4)
    print("moving json file to backup folder")
    move(f'{home}\\Documents\\destination.json', f'{home}\\Documents\\backup\\destination.json') #moves json file to backup folder
    print("Creating archive")
    make_archive('backup','zip',f'{home}\\Documents\\backup\\') #creates zip file
    print("Archive made")
    print("Removing backup folder") #removes temporary backup folder
    rmtree(f'home\\Documents\\backup')

def restore(backup_location):
    data = {}
    
    extract_dir=os.path.splitext(backup_location)[0] #separates the extension from the path for extracting
    unpack_archive(backup_location, extract_dir, "zip") #unpacks the zip folder
    print("unpacked archive")
    os.chdir(os.path.splitext(backup_location)[0]) #change working directory to extracted zip
    with open("destination.json") as file: #puts the data from the json file in a library
        data = json.load(file)
    print(data)
    for item in data:
        print(f'Moving {item} to {data[item]}') #goes over every item in the library and restores the files to their original position
        file_source = os.getcwd() + "\\" + item
        file_destination = data[item]
        move(file_source, file_destination)
while True: 
    event, values = window.read() #starts the GUI
    if(event == sg.WIN_CLOSED): #stops loop if window was closed
        break
    elif(event == 'Add Folder'): #adds path in input field to list if button was pressed
        path_list.append(window["-INPUT-"].get())
        window.FindElement('-FILE LIST-').Update(values=path_list) #updates list of added paths in GUI
    elif(event == "Export"): #starts export process if button is pressed
        export(path_list, extensions_list)
    elif(event == "Add Extension"): #adds extension to list
        extensions_list.append( '*' + window["-EXTENSIONS INPUT-"].get())
        window.FindElement('-EXTENSIONS LIST-').Update(values=extensions_list) #updates gui with list of added extension to ignore
    elif(event == "Restore"):
        backup = window["-BACKUP LOCATION-"].get() #adds backup location from input field to variable
        restore(backup) #starts restoration process
